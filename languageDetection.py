#import libraries
import numpy as np
import pandas as pd
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split

#External database used 
data = pd.read_csv("https://raw.githubusercontent.com/amankharwal/Website-data/master/dataset.csv")


#Teaching the algorithm to follow Multinomial Naive Bayes techiques
x = np.array(data["Text"])
y = np.array(data["language"])
cv = CountVectorizer()
X = cv.fit_transform(x)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)

model = MultinomialNB()
model.fit(X_train,y_train)
model.score(X_test,y_test)

#Creating default value while statement
enterInput = '1'
#While statement to allow for option to enter more text
while enterInput == '1':
    #User input 
    user = input("Enter Text: ")
    data = cv.transform([user]).toarray()
    output = model.predict(data)
    print(output)
    #Repitition option
    print('~~~~~~~~~~~~~~~')
    enterInput = input("Enter 1 to continue: ")
    print('~~~~~~~~~~~~~~~')


    
#Below is the code used during testing phase
#data = cv.transform(["Hey my name is max"]).toarray() #English
#output = model.predict(data)
#print(output)
#data = cv.transform(["Salut je m'appelle max"]).toarray() #French 
#output = model.predict(data)
#print(output)
#data = cv.transform(["अरे मेरा नाम मैक्स है"]).toarray() #Hindi
#output = model.predict(data)
#print(output)
#data = cv.transform(["hey benim adım max"]).toarray() #Turkish
#output = model.predict(data)
#print(output)
#data = cv.transform(["Γεια, το όνομά μου είναι max"]).toarray() #Greek**
#output = model.predict(data)
#print(output)
#data = cv.transform(["Hej mam na imię maks"]).toarray() #Polish**
#output = model.predict(data)
#print(output)
